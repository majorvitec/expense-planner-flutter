import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteHandler;

  TransactionList(this.transactions, this.deleteHandler);

  Card _transactionCard(Transaction transaction, BuildContext context) {
    return Card(
        child: Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 10,
          ),
          child: Text(
            '${transaction.amount.toStringAsFixed(2)} €',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
          decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(context).primaryColor,
              width: 2,
            ),
          ),
          padding: EdgeInsets.all(5),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              transaction.title,
              style: Theme.of(context).textTheme.headline6,
            ),
            Text(
              DateFormat.yMMMd().format(transaction.date),
              style: TextStyle(
                color: Colors.grey,
                fontSize: 12,
              ),
            )
          ],
        )
      ],
    ));
  }

  ListView _buildTranactions(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) =>
          _transactionCard(transactions[index], context),
      itemCount: transactions.length,
    );
  }

  FlatButton _createTextIconButton(BuildContext context, int index) {
    return FlatButton.icon(
      icon: Icon(Icons.delete),
      label: Text('Delete'),
      textColor: Theme.of(context).errorColor,
      onPressed: () => deleteHandler(transactions[index].id),
    );
  }

  IconButton _createIconButton(BuildContext context, int index) {
    return IconButton(
        icon: Icon(Icons.delete),
        color: Theme.of(context).errorColor,
        onPressed: () {
          deleteHandler(transactions[index].id);
        });
  }

  ListView _buildTransactionsRoundStyle(BuildContext context) {
    final double _screenWidth = MediaQuery.of(context).size.width;
    final bool _bigScreen = _screenWidth > 460;

    return ListView.builder(
      itemBuilder: (context, index) {
        return Card(
          elevation: 5,
          margin: EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 5,
          ),
          child: ListTile(
            leading: CircleAvatar(
              radius: 30,
              child: Padding(
                padding: EdgeInsets.all(6),
                child: FittedBox(
                  child: Text(
                    '${transactions[index].amount.toStringAsFixed(2)} €',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
            ),
            title: Text(
              transactions[index].title,
              style: Theme.of(context).textTheme.headline6,
            ),
            subtitle: Text(
              DateFormat.yMMMd().format(transactions[index].date),
            ),
            trailing: _bigScreen
                ? _createTextIconButton(context, index)
                : _createIconButton(context, index),
          ),
        );
      },
      itemCount: transactions.length,
    );
  }

  LayoutBuilder _noTransaction(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final availableHeight = constraints.maxHeight;
        final spaceHeight = availableHeight * 0.1;
        final imageHeight = availableHeight * 0.6;

        return Column(
          children: <Widget>[
            Text(
              'No Transaction set!',
              style: Theme.of(context).textTheme.headline6,
            ),
            SizedBox(
              height: spaceHeight,
            ),
            Container(
              height: imageHeight,
              child: Image.asset(
                'assets/images/waiting.png',
                fit: BoxFit.cover,
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      child: transactions.isEmpty
          ? _noTransaction(context)
          : _buildTransactionsRoundStyle(context),
    );
  }
}
