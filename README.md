# Expense Planner [2019]

Build a Expense Planner App [Followed Tutorial].

## Dependencies
- [intl](https://pub.dev/packages/intl)

## App Overview

### Expense Planner Screen

![Expense Planner Screen](/images/readme/expense_planner.png "Expense Planner Screen")

### Adding Expense Screen

![Adding Expense Screen](/images/readme/add_expense.png "Adding Expense Screen")

### Expense Added Screen

![Expense Added Screen](/images/readme/expense_added.png "Expense Added Screen")

